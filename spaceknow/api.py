from PIL import Image
import geojson
import json
import math

from spaceknow.net import Net

SK_IMAGERY_SEARCH_URL = 'https://spaceknow-imagery.appspot.com/imagery/search'
SK_SEGMENTATION_AIRCRAFT_URL = 'https://spaceknow-object-detection.appspot.com/vidar/segmentation/aircraft'
SK_VISUALIZATION_MASK_URL = 'https://spaceknow-visualization.appspot.com/visualization/segmentation-masks'
SK_KRAKEN_SCENE_URL = 'https://spaceknow-kraken.appspot.com/kraken/release/scene'
SK_KRAKEN_GRID_URL = 'https://spaceknow-kraken.appspot.com/kraken/grid'

SK_MIN_AIRCRAFT_AREA = 100

class Api:
    def __init__(self, verbose):
        self._net = Net(verbose)
        self.is_verbose = verbose

    def authenticate(self, login, password):
        self._net.authenticate(login, password)

    def load_scenes(self, provider, dataset, extent):
        data = {
            'provider': provider,
            'dataset': dataset,
            'extent': extent
        }
        return self._net.async_api_request(SK_IMAGERY_SEARCH_URL, data)

    def detect_aircrafts(self, scene, extent, color=[255, 0, 0]):
        data = {
            'sceneId': scene,
            'extent': extent,
        }
        result = self._net.async_api_request(SK_SEGMENTATION_AIRCRAFT_URL, data)
        file_id = result['fileId']
        geojson_file_id = result['detectionGeoJsonFileId']

        def is_aircraft(props):
            return props['class'] == 'aircraft' and props['area'] > SK_MIN_AIRCRAFT_AREA

        geojson = self._net.download_json(geojson_file_id)
        count = sum([1 for i in geojson['features'] if is_aircraft(i['properties'])])

        data = {
            'fileId': file_id,
            'visualizationParameters': [ { 'name': 'aircraft', 'rgb': color } ]
        }
        result = self._net.async_api_request(SK_VISUALIZATION_MASK_URL, data)
        image = self._net.download_image(result['visualizationFileId'])

        return { 'image': image, 'count': count, 'extent': extent }

    def build_orthophoto(self, scene_id, extent, zoom):
        bbox = self._compute_bbox(extent)
        tiles = self._compute_tiles_for_bbox(bbox, zoom)

        data = {
            'sceneId': scene_id,
            'tiles': tiles['tiles']
        }
        result = self._net.async_api_request(SK_KRAKEN_SCENE_URL, data)
        map_id = result['mapId']
        image = self._combine_tiles_to_image(map_id, tiles)

        tiles_bbox = self._compute_tiles_bbox(tiles, zoom)

        return { 'image': image, 'bbox': tiles_bbox }

    def _compute_bbox(self, extent):
        coords = self._extract_coords(extent)
        top = right = float('-inf')
        bottom = left = float('inf')
        for point in coords:
            top = max(top, point[1])
            bottom = min(bottom, point[1])
            left = min(left, point[0])
            right = max(right, point[0])
        return { 'left': left, 'top': top, 'right': right, 'bottom': bottom }

    def _extract_coords(self, extent):
        json_text = json.dumps(extent)
        extent_as_geojson = geojson.loads(json_text)
        # NB list(geojson.utils.coords(extent_as_geojson)) does not work for some reason
        coords = []
        if isinstance(extent_as_geojson, geojson.GeometryCollection):
            coords = reduce((lambda x, y: x + y), reduce((lambda x, y: x + y), map(
                (lambda geometry: geometry.coordinates), extent_as_geojson.geometries
            )))
        elif isinstance(extent_as_geojson, geojson.Feature):
            coords = reduce((lambda x, y: x + y), map(
                (lambda geometry: geometry.coordinates), extent_as_geojson.geometry
            ))
        elif isinstance(extent_as_geojson, geojson.FeatureCollection):
            coords = reduce((lambda x, y: x + y), reduce((lambda x, y: x + y), map(
                (lambda geometry: geometry.coordinates),
                map((lambda feature: feature.geometry), extent_as_geojson.features)
            )))
        return coords

    def _compute_tiles_for_bbox(self, bbox, zoom):
        tl_tile = self._deg_to_tile_coords(bbox['top'], bbox['left'], zoom=zoom)
        br_tile = self._deg_to_tile_coords(bbox['bottom'], bbox['right'], zoom=zoom)
        tiles = []
        for y in range(tl_tile['y'], br_tile['y'] + 1):
            for x in range(tl_tile['x'], br_tile['x'] + 1):
                tiles.append([ zoom, x, y ])
        return {
            'tiles': tiles,
            'left': tl_tile['x'],
            'top': tl_tile['y'],
            'x': br_tile['x'] - tl_tile['x'] + 1,
            'y': br_tile['y'] - tl_tile['y'] + 1,
        }

    def _deg_to_tile_coords(self, lat, lon, zoom):
        lat_rad = math.radians(lat)
        n = 2.0 ** zoom
        x = int((lon + 180.0) / 360.0 * n)
        y = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
        return { 'x': x, 'y': y }

    def _combine_tiles_to_image(self, map_id, tiles):
        combined_image = Image.new('RGBA', (tiles['x']*256, tiles['y']*256))

        for tile in tiles['tiles']:
            tile_img = self._download_tile(map_id, *tile)
            img_x = 256*(tile[1] - tiles['left'])
            img_y = 256*(tile[2] - tiles['top'])
            combined_image.paste(tile_img, (img_x, img_y))
        return combined_image

    def _download_tile(self, map_id, zoom, x, y):
        url = SK_KRAKEN_GRID_URL + '/{}/-/{}/{}/{}.png'.format(map_id, zoom, x, y)
        return self._net.get_image(url)

    def _compute_tiles_bbox(self, tiles, zoom):
        (top, left) = self._tile_coords_to_deg(tiles['left'], tiles['top'], zoom)
        (bottom, right) = self._tile_coords_to_deg(tiles['left'] + tiles['x'], tiles['top'] + tiles['y'], zoom)
        return { 'left': left, 'top': top, 'right': right, 'bottom': bottom }

    def _tile_coords_to_deg(self, x, y, zoom):
        n = 2.0 ** zoom
        lon = x / n * 360.0 - 180.0
        lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * y / n)))
        lat = math.degrees(lat_rad)
        return (lat, lon)

    def combine_orthophoto_with_aircrafts(self, orthophoto, aircrafts):
        air_image = aircrafts['image']
        air_bbox = self._compute_bbox(aircrafts['extent'])
        ortho_image = orthophoto['image']
        ortho_bbox = orthophoto['bbox']

        air_x_size = air_bbox['right'] - air_bbox['left']
        air_y_size = air_bbox['top'] - air_bbox['bottom']
        air_y_scale = air_image.height / air_y_size
        air_x_scale = air_image.width / air_x_size

        ortho_x_size = ortho_bbox['right'] - ortho_bbox['left']
        ortho_y_size = ortho_bbox['top'] - ortho_bbox['bottom']
        ortho_y_scale = ortho_image.height / ortho_y_size
        ortho_x_scale = ortho_image.width / ortho_x_size

        x_mult = ortho_x_scale / air_x_scale
        y_mult = ortho_y_scale / air_y_scale

        resized_air_image = air_image.resize((
            int(round(air_image.width * x_mult)),
            int(round(air_image.height * y_mult))
        ))

        top_offset = int(round((ortho_bbox['top'] - air_bbox['top']) * ortho_y_scale))
        left_offset = int(round((air_bbox['left'] - ortho_bbox['left']) * ortho_x_scale))
        cropped_ortho_image = ortho_image.crop((
            left_offset,
            top_offset,
            left_offset + resized_air_image.width,
            top_offset + resized_air_image.height
        ))

        return Image.alpha_composite(cropped_ortho_image, resized_air_image)
