from PIL import Image
from StringIO import StringIO
import httplib
import requests
import time

SK_AUTH_URL = 'https://spaceknow.auth0.com/oauth/ro'
SK_TASK_STATUS_URL = 'https://spaceknow-tasking.appspot.com/tasking/get-status'
SK_FILE_DOWNLOAD_URL = 'https://spaceknow-files.appspot.com/download'

class ApiException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Net:
    def __init__(self, verbose):
        self._id_token = None
        self._is_verbose = verbose

    def authenticate(self, login, password):
        data = {
            'client_id': 'Qqdwx21Ocn2Yj0q3ANunnXVp3IYE0uhW',
            'username': login,
            'password': password,
            'connection': 'Username-Password-Authentication',
            'grant_type': 'password',
            'scope': 'openid'
        }
        result = self.post_with_auth(SK_AUTH_URL, data)
        self._id_token = result['id_token']

    def async_api_request(self, url, data):
        combined_result = []

        self._verbose('Waiting for: ' + url)
        while True:
            result = self.post_with_auth(url + '/initiate', data)
            pipeline_id = result['pipelineId']
            self.poll_task(pipeline_id)
            result = self.post_with_auth(url + '/retrieve', {'pipelineId': pipeline_id})
            if 'cursor' in result and 'results' in result:
                combined_result += result['results']
                if result['cursor']:
                    data['cursor'] = result['cursor']
                    continue
                return combined_result
            return result

    def poll_task(self, pipeline_id):
        while True:
            data = self.post_with_auth(SK_TASK_STATUS_URL, { 'pipelineId' : pipeline_id })
            if data['status'] != 'NEW' and data['status'] != 'PROCESSING':
                break
            time.sleep(1)
        return data['status']

    def post_with_auth(self, url, json, parse_result=True):
        #httplib.HTTPConnection.debuglevel = 1
        headers = {}
        if self._id_token:
            headers['Authorization'] = 'Bearer ' + self._id_token
        response = requests.post(url, json=json, headers=headers)
        self._check_response(response)
        return response.json() if parse_result else response

    def download_image(self, file_id):
        result = self.post_with_auth(SK_FILE_DOWNLOAD_URL, { 'fileId': file_id }, parse_result=False)
        return Image.open(StringIO(result.content))

    def get_image(self, url):
        response = requests.get(url)
        self._check_response(response)
        return Image.open(StringIO(response.content))

    def download_json(self, file_id):
        return self.post_with_auth(SK_FILE_DOWNLOAD_URL, { 'fileId': file_id })

    def _check_response(self, response):
        if not response.ok:
            raise ApiException({
                'url': response.url,
                'status': response.status_code,
                'text': response.text
            })

    def _verbose(self, msg):
        if self._is_verbose:
            print(msg)
