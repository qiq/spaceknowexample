#!/usr/bin/env python
import argparse
import json
import os
import re
from spaceknow.api import Api;
from spaceknow.net import ApiException;

def main():
    parser = argparse.ArgumentParser(description='Detect objects on maps using SpaceKnow API.')
    parser.add_argument('--config', required=True, help='json with SpaceKnow API credentials and provider configuration')
    parser.add_argument('--extent', required=True, help='geometry in geojson format')
    parser.add_argument('--image', required=True, help='output png image', default='output.png')
    parser.add_argument('--zoom', help='zoom of orthophoto', default=17)
    parser.add_argument('--verbose', help='increase output verbosity', action='store_true')
    args = parser.parse_args()

    with open(args.config) as json_data:
            config = json.load(json_data)
    with open(args.extent) as json_data:
            geojson = json.load(json_data)
    try:
        detect(config, geojson, args.image, args.zoom, args.verbose)
    except ApiException as e:
        print 'Error processing request:', e.value

def detect(config, geojson, image_filename, zoom, verbose=False):
    """ detect processes all scenes available for all providers and datasets
        configured, prints count of aircrafts found for geojson-defined area of
        every scene avaliable. Also wirtes file of ortophoto with yellow-marked
        aircrafts. Zoom may be used to adjust resolution of the orthophoto.
    """

    spaceknow = Api(verbose)
    spaceknow.authenticate(config['auth']['login'], config['auth']['password'])
    for provider in config['providers']:
        provider_name = provider['name']
        for dataset in provider['datasets']:
            try:
                scenes = spaceknow.load_scenes(provider_name, dataset, geojson)
            except ApiException as e:
                print 'Invalid provider/dataset:', e.value['text']
                continue
            sorted_scenes = sorted(scenes, key=lambda scene: scene['datetime'])
            previous_datetime = None
            for scene in sorted_scenes:
                scene_id = scene['sceneId']
                datetime = scene['datetime']
                if verbose:
                    print 'Processing scene {}, dataset: {}/{}'.format(datetime, provider_name, dataset)
                if datetime == previous_datetime:
                    print 'Skipped, the same timestamp as in previous scene'
                    continue
                else:
                    previous_datetime = datetime

                try:
                    (count, image) = _detect_in_scene(spaceknow, scene_id, geojson, zoom)
                except ApiException as e:
                    print 'Skipped, error processing scene: ', e.value['text']
                    continue
                filename = _construct_output_filename(image_filename, datetime, provider_name, dataset)
                image.save(filename)
                print('Timestamp: {}, dataset: {}/{}, aircraft count: {}, image: {}'
                    .format(datetime, provider_name, dataset, count, filename)
                )

def _detect_in_scene(spaceknow, scene_id, geojson, zoom):
    """ detects aircrafts in geojson-specified area of the scene
    """

    aircrafts = spaceknow.detect_aircrafts(scene_id, geojson, color=[255, 255, 0])
    orthophoto = spaceknow.build_orthophoto(scene_id, geojson, zoom=zoom)
    return (
        aircrafts['count'],
        spaceknow.combine_orthophoto_with_aircrafts(orthophoto, aircrafts),
    )

def _construct_output_filename(template, date, provider, dataset):
    (prefix, suffix) = os.path.splitext(template)
    if not suffix:
        suffix = '.png'
    date = re.sub(r'[^\w:-]', '_', date)
    return '{}_{}_{}_{}{}'.format(prefix, provider, dataset, date, suffix)

if __name__ == '__main__':
    main()
